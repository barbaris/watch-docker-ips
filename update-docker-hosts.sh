# Remove all lines with `docker-ips` from file `/etc/hosts`
#sed -i -r '/docker-ips/d' /etc/hosts
cat /etc/hosts | grep -v docker-ips > /tmp/hosts.txt
date > /tmp/docker-hosts.log
sleep 1
echo \# docker-ips $(date) >> /tmp/hosts.txt
cat /tmp/docker-hosts >> /tmp/hosts.txt
cp /tmp/hosts.txt /etc/hosts
