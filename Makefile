install: install-binaries install-service

install-watcher: install-nodemon
	@echo "Installing watcher"
	sudo cp watch-docker-hosts.sh /usr/local/bin/watch-docker-hosts
	sudo chmod +x /usr/local/bin/watch-docker-hosts

install-updater:
	@echo "Installing updater"
	sudo cp update-docker-hosts.sh /usr/local/bin/update-docker-hosts
	sudo chmod +x /usr/local/bin/update-docker-hosts

install-ips:
	@echo "Installing docker-ips"
	sudo cp docker-ips /usr/local/bin/docker-ips
	sudo chmod +x /usr/local/bin/docker-ips
	sudo cp docker-ps /usr/local/bin/docker-ps
	sudo chmod +x /usr/local/bin/docker-ps

install-service: stop-service
	@echo "Installing service"
	sudo cp watch-docker-hosts.service /etc/systemd/system
	sudo systemctl enable watch-docker-hosts.service
	sudo systemctl start watch-docker-hosts.service
	sudo systemctl status watch-docker-hosts.service

install-binaries: install-watcher install-updater install-ips

install-nodemon:
	@echo "Checking npm version" && npm version && echo "npm is installed. Nothing to do here" || pamac install npm --no-confirm
	@echo "Checking nodemon version" && nodemon --version && echo "nodemon is installed. Nothin to do here" || npm install -g nodemon

stop-service:
	@echo "Stopping service"
	sudo systemctl stop watch-docker-hosts.service || echo "Service not running"
	sudo systemctl disable watch-docker-hosts.service || echo "Service not enabled"
